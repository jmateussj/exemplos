package com.example.josesilverio.teste3;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class clsTeste{
    @SerializedName("ret")
    @Expose
    public String ret;
    @SerializedName("t")
    @Expose
    public String t;

    @Override
    public String toString() {
        return "clsTeste{" +
                "ret='" + ret + '\'' +
                ", t='" + t + '\'' +
                '}';
    }
}


