package com.example.josesilverio.teste3;

/**
 * Created by Jose.Silverio on 22/03/2018.
 */
import com.google.gson.annotations.SerializedName;
import  com.google.gson.annotations.Expose;

public class clsOperadora {

    @SerializedName("operadora_nome")
    @Expose
    public String operadora_nome;
    @SerializedName("operadora_id")
    @Expose
    public int operadora_id;
    @SerializedName("operadora_ativo")
    @Expose
    public int operadora_ativo;

    @Override
    public String toString() {
        /*return "clsOperadora{" +
                "operadora_nome='" + operadora_nome + '\'' +
                ", operadora_id=" + operadora_id +
                ", operadora_ativo=" + operadora_ativo +
                '}';*/
        return this.operadora_nome;
    }
}

