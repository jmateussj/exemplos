package com.example.josesilverio.teste3;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jose.Silverio on 22/03/2018.
 */

public class RetroFitConfig {
    Retrofit retro;
    static String URL_BASE="http://siavconsultora.com.br/wp-json/mksys/";
    public RetroFitConfig()
    {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        retro = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

    }
    public MKinterfaces getTeste()
    {
        return this.retro.create(MKinterfaces.class);
    }
    public MKinterfaces getToken()
    {
        return this.retro.create(MKinterfaces.class);
    }
    public MKinterfaces getOperadoras()
    {
        return this.retro.create(MKinterfaces.class);
    }
}
