package com.example.josesilverio.teste3;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Jose.Silverio on 22/03/2018.
 */

public interface MKinterfaces {
    @GET("teste")
    Call<clsTeste>getTeste();

    @POST("token")
    Call<clsToken>Token(@Header("Authorization") String basico);

    @GET("auxiliares/operadora")
    Call<clsOperadora[]>Operadoras(@Header("Authorization") String bearer);





}
