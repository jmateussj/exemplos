package com.example.josesilverio.teste3;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class clsToken{
    /*{
        "token": "string",
            "expires": "string",
            "user_email": "string",
            "user_nicename": "string",
            "user_display_name": "string"
    }*/

    @SerializedName("token")
    @Expose
    public String token;
    @SerializedName("expires")
    @Expose
    public String expires;
    @SerializedName("user_mail")
    @Expose
    public String user_mail;
    @SerializedName("user_nicename")
    @Expose
    public String user_nicename;
    @SerializedName("user_diaplay_name")
    @Expose
    public String user_display_name;

    @Override
    public String toString() {
        return "clsToken{" +
                "token='" + token + '\'' +
                //", expires='" + expires + '\'' +
                //", user_mail='" + user_mail + '\'' +
                //", user_nicename='" + user_nicename + '\'' +
                //", user_display_name='" + user_display_name + '\'' +
                '}';
    }
}
