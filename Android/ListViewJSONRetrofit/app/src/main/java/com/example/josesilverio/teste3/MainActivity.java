package com.example.josesilverio.teste3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


    static String token = "";
    ListView listaOperadoras;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaOperadoras = findViewById(R.id.listaOperadoras);

        String b = "";
        String kk ="";
        try {
            kk = Base64.encodeToString(String.format("%s:%s", "admin", "*******************").getBytes("UTF-8"),Base64.NO_WRAP);
            Log.d("mofuseadoba",kk);
            //b="Basic " + Base64.encodeToString(String.format("%s:%s", "admin", "*******************").getBytes("UTF-8"),Base64.DEFAULT);
        }
        catch(UnsupportedEncodingException e)
        {
            Log.e("mofuseadoba",e.getMessage());
        }



        b = "Basic YWRtaW46TmF1bXNlaTEyMzQh";
        kk = String.format("Basic %s",kk);

        Log.d("mofuseadoba","valor de  b: " + b);
        Log.d("mofuseadoba","valor de kk: " + kk);

        Call<clsToken> call = new RetroFitConfig().getToken().Token(kk);
        call.enqueue(new Callback<clsToken>() {
            @Override
            public void onResponse(Call<clsToken> call, Response<clsToken> response) {
                Log.d("mofuseadoba",response.message());
                clsToken tkn = response.body();
                Log.d("mofuseadoba",tkn.toString());
                token = tkn.token;

                String bearer = String.format("Bearer %s",token);
                Call<clsOperadora[]> call2 = new RetroFitConfig().getOperadoras().Operadoras(bearer);
                call2.enqueue(new Callback<clsOperadora[]>() {
                    @Override
                    public void onResponse(Call<clsOperadora[]> call, Response<clsOperadora[]> response) {
                        clsOperadora[] arr = response.body();
                        for(int i=0; i<arr.length;i++){
                            Log.d("mofuseadoba",arr[i].toString());
                        }
                        ArrayAdapter<clsOperadora> adp = new ArrayAdapter<clsOperadora>(
                                MainActivity.this,
                                android.R.layout.simple_list_item_1,
                                arr
                        );
                        listaOperadoras.setAdapter(adp);

                    }

                    @Override
                    public void onFailure(Call<clsOperadora[]> call, Throwable t) {

                    }
                });


            }

            @Override
            public void onFailure(Call<clsToken> call, Throwable t) {

                Log.e("mofuseadoba","Falha ao fazer a chamada: "+t.getMessage());
            }

        });

        /*
        Call<clsTeste> c = new RetroFitConfig().getTeste().getTeste();

        c.enqueue(new Callback<clsTeste>() {
            @Override
            public void onResponse(Call<clsTeste> call, Response<clsTeste> response) {
                clsTeste t = response.body();
                Log.d("mofuseadoba",t.toString());
            }

            @Override
            public void onFailure(Call<clsTeste> call, Throwable t) {
                Log.d("mofuseadoba",t.getMessage());

            }
        });*/


    }
}

