package com.example.josesilverio.teste02;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<clsOperadora> operadoras;
    private ProgressBar pb;
    private ListView lista;
    public static String urlToken="http://siavconsultora.com.br/wp-json/mksys/token";
    public static String urlOperadora = "http://siavconsultora.com.br/wp-json/mksys/auxiliares/operadora";
    public static String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        operadoras = new ArrayList<clsOperadora>();

        lista = (ListView) findViewById(R.id.listviewOperadoras);

        new OperadorasReader().execute();
    }


    private class OperadorasReader extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            Log.d("mofuseadoba","Iniciando Execução");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ArrayAdapter<clsOperadora> adp = new ArrayAdapter<clsOperadora>(
                    MainActivity.this,
                    android.R.layout.simple_list_item_1,
                    operadoras
            );
            lista.setAdapter(adp);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                httpHandler http = new httpHandler();

                String k = Base64.encodeToString(String.format("%s:%s", "admin", "*******************").getBytes("UTF-8"),Base64.DEFAULT);

                String auth = String.format("Basic %s",k);
                String strjsontoken = http.makeServiceCall(urlToken, "POST", auth);
                Log.d("mofuseadoba",strjsontoken);

                JSONObject jo = new JSONObject(strjsontoken);
                token = jo.getString("token");

                auth = String.format("Bearer %s",token);

                strjsontoken = http.makeServiceCall(urlOperadora,"GET",auth);
                Log.d("mofuseadoba",strjsontoken);

                JSONArray arrOperadoras = new JSONArray(strjsontoken);
                for(int i=0; i<arrOperadoras.length();i++) {

                    JSONObject operadora = arrOperadoras.getJSONObject(i);

                    clsOperadora op = new clsOperadora();
                    op.operadora_ativo = operadora.getInt("operadora_ativo");
                    op.operadora_id = operadora.getInt("operadora_id");
                    op.operadora_nome = operadora.getString("operadora_nome");


                    //String operadoraNome = operadora.getString("operadora_nome");

                    Log.d("mofuseadoba", "Adicionando: "+op.operadora_nome);
                    operadoras.add(op);
                }



            }
            catch(Exception ex)
            {
                Log.d("mofuseadoba",ex.getMessage());
            }
            return null;

        }
    }
}