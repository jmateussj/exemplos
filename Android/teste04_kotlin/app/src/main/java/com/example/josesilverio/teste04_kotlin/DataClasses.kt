package com.example.josesilverio.teste04_kotlin

/**
 * Created by Jose.Silverio on 22/03/2018.
 */
object Modelos {
    data class clsTeste(var ret: String, var t: String)
    data class clsToken(
            var token: String,
            var expires: String,
            var user_mail: String,
            var user_nicename: String,
            var user_display_name: String
    )
    data class clsOperadora(
        var operadora_nome: String,
        var operadora_id: Int,
        var operadora_ativo: Int

    ) {
        override fun toString(): String {
            return operadora_nome
        }
    }

}

