package com.example.josesilverio.teste04_kotlin

import retrofit2.Retrofit
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.converter.gson.GsonConverterFactory
/**
 * Created by Jose.Silverio on 22/03/2018.
 */

class RetroFitConfig{
    val BASE_URL: String = "http://siavconsultora.com.br/wp-json/mksys/"
    val retro = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .build()


    fun getEndpoints(): MKInterface{
        return retro.create(MKInterface::class.java)
    }
}