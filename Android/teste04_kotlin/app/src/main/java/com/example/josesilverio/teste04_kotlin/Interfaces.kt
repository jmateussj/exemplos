package com.example.josesilverio.teste04_kotlin

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import java.util.*

/**
 * Created by Jose.Silverio on 22/03/2018.
 */
interface MKInterface{
    @GET("teste")
    fun getTeste(): Call<Modelos.clsTeste>

    @POST("token")
    fun getToken(@Header("Authorization") basico: String): Call<Modelos.clsToken>

    @GET("auxiliares/operadora")
    fun getOperadoras(@Header("Authorization") bearer: String): Call<List<Modelos.clsOperadora>>


}