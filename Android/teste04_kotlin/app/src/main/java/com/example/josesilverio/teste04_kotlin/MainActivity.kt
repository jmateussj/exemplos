package com.example.josesilverio.teste04_kotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListView
import com.google.gson.internal.bind.ArrayTypeAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    var tkn :String =""
    var listaOperadoras: ListView? = null // = findViewById(R.id.listOperadoras)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listaOperadoras = findViewById(R.id.listOperadoras)

        var kk = Base64.encodeToString(String.format("%s:%s", "admin", "Naumsei1234!").toByteArray(Charsets.UTF_8), Base64.NO_WRAP)

        val basico = String.format("Basic %s", kk)

        val call = RetroFitConfig().getEndpoints().getToken(basico)
        call.enqueue(object : Callback<Modelos.clsToken> {

            override fun onResponse(call: Call<Modelos.clsToken>?, response: Response<Modelos.clsToken>?) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                response?.let {
                    val t: Modelos.clsToken = response.body()
                    Log.d("mofuseadoba", t.token)
                    tkn = t.token

                }

                val bearer: String = String.format("Bearer %s",tkn)
                val call2 = RetroFitConfig().getEndpoints().getOperadoras(bearer)
                call2.enqueue(object: Callback<List<Modelos.clsOperadora>>{
                    override fun onResponse(call: Call<List<Modelos.clsOperadora>>?, response: Response<List<Modelos.clsOperadora>>?) {
                        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        response?.let{
                            val oprs=response.body()
                            for(o in oprs)
                            {
                                Log.d("mofuseadoba",o.operadora_nome)
                            }

                            listaOperadoras?.adapter = ArrayAdapter(
                                    this@MainActivity,
                                    android.R.layout.simple_list_item_1,
                                    oprs
                            )

                        }

                    }

                    override fun onFailure(call: Call<List<Modelos.clsOperadora>>?, t: Throwable?) {
                        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        Log.e("mofuseadoba", t?.message)
                    }
                })



            }

            override fun onFailure(call: Call<Modelos.clsToken>?, t: Throwable?) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                Log.e("mofuseadoba", t?.message)
            }

        })
    }
}

/*
        val call = RetroFitConfig().getEndpoints().getTeste()
        call.enqueue(object: Callback<Modelos.clsTeste>{
            override fun onResponse(call: Call<Modelos.clsTeste>?, response: Response<Modelos.clsTeste>?) {
                response?.let {
                    val t = response.body()
                    Log.d("mofuseadoba",t.ret)

                }




            }
            override fun onFailure(call: Call<Modelos.clsTeste>?, t: Throwable?) {
                Log.d("mofuseadoba",t?.message)
            }
        })
        /*
        call.enqueue(object : Callback<Modelos.clsTeste>?){

        }*/

    }
}


*/