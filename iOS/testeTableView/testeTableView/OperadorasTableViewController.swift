//
//  OperadorasTableViewController.swift
//  testeTableView
//
//  Created by José Mateus Silverio Jr on 20/03/2018.
//  Copyright © 2018 José Mateus Silverio Jr. All rights reserved.
//

import UIKit

class OperadorasTableViewController: UITableViewController {
    
    var dataSource:[String] = [] //["linha 1", "linha 2", "linha 3", "linha 4"]
    var token: String=""
    
    @IBOutlet var operadorasTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Autenticar() //autentica e carrega
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataSource.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        cell.textLabel?.text = dataSource[indexPath.row]
        
        return cell
    }
    func Autenticar(){
        let urlChamada = "http://siavconsultora.com.br/wp-json/mksys/token"
        let url = URL(string: urlChamada)
        var req = URLRequest(url: url!)
        let usuario="admin"
        let pqd = "***************"
        let xx = String(format: "%@:%@",usuario,pqd)
        let autorization: String = String.init(format: "Basic %@",xx.data(using: .utf8)!.base64EncodedString())
        req.httpMethod="POST"
        req.setValue(autorization, forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        session.dataTask(with: req){
            (data,response,error) in
            guard error==nil else{
                print(error!)
                return
            }
            let resposta = response as? HTTPURLResponse
            print("StatusCode \(resposta!.statusCode)")
            if let dados = data{
                do{
                    let tkn = try JSONSerialization.jsonObject(with: dados, options: .mutableContainers) as? [String:Any]
                    guard let token = tkn!["token"] else{
                        print("Falha ao ler o token")
                        return
                    }
                    
                    DispatchQueue.main.sync {
                        self.token = token as! String
                        self.CarregarOperadoras(tkn: self.token)
                    }
                    
                }
                catch{
                    print(error)
                    return
                }
            }
            
        }.resume()
        
    }
    func CarregarOperadoras(tkn: String){
        
        if(tkn=="")
        {
            print("Sem Token")
            return
        }
        let chamada: String = "http://siavconsultora.com.br/wp-json/mksys/auxiliares/operadora"
        debugPrint(chamada)
        
        guard let url = URL(string: chamada) else{
            return
        }
        
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod="GET"
        urlRequest.setValue(String(format:"Bearer %@",tkn), forHTTPHeaderField: "Authorization")
        let session = URLSession.shared
        
        session.dataTask(with: urlRequest){
            (data,response,error) in
            
            guard error==nil else
            {
                print(error!)
                return
            }
            let codigoResposta = response as? HTTPURLResponse
            print("Código de resposta \(codigoResposta!.statusCode)")
            print("=============================================================")
            debugPrint(data)
            let retornoSTR = String(data: data!, encoding: .utf8)
            debugPrint(retornoSTR)
            print("=============================================================")
            do{
                //let clientesObj = try JSONDecoder().decode([clsOperadoras].self, from: data!)
                //print(clientesObj)
                let operadoras = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [[String:Any]]
                var lista:[String]=[]
                for o in operadoras!
                {
                    lista.append(o["operadora_nome"] as! String)
                }
                DispatchQueue.main.sync {
                    self.dataSource=lista
                    self.operadorasTableView.reloadData()
                    
                }
                
            }
            catch{
                print(error)
            }
            /*
            if let resposta = data{
                do{
                    let clientes = try JSONSerialization.jsonObject(with: resposta, options: .mutableContainers) as! [[String: Any]]
                    
                    
                    //print(clientes)
                    for c in clientes{
                        guard let k = c["operadora_nome"]
                            else{
                                print("Falha ao ler conteudo do array de json")
                                return
                        }
                        /*if let obj = c as? clsOperadoras{
                         print("cast ok")
                         print(obj)
                         }
                         else{
                         print("fail")
                         }*/
                        print(k)
                    }
                    
                }
                catch{
                    print("Falha ao processar o JSON")
                    let saida = String(data: resposta, encoding: .utf8)
                    print(saida)
                }
                
            }*/
            }.resume()
        
        
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
